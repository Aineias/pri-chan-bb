# 試験版プリ☆チャンBB素材GitLabリポジトリ

◇GitLabのページ     https://gitlab.com/Aineias/pri-chan-bb <br/>
◇URL	    	    git@gitlab.com:Aineias/pri-chan-bb.git<br/>
◇連絡先		        https://twitter.com/aineias_stymph


## **FAQ**

Q.　なにこれ？（TR）\
A.　プリチャンのBB素材をGitで配布する試みです

Q.　Gitってなんだよ（女児として当然な疑問）\
A.　エンジニアがよく使うバージョン管理システムとかいうアレです
	
Q.　んにゃぴ……\
A.　要するに素材パックの配布と更新が楽になります


Gitを使った素材配布の利点を挙げると：
* 	素材の追加が簡単になる（フォルダにぶち込んでチェックインするだけで済む）
* 	更新があった際、ユーザーは差分だけをダウンロードすることができる（毎回全部の素材をダウンロードせずに済む）
* 	ユーザーも素材を追加できる

Q.　そうなんだ（無関心）\
A.　いいから使え

## **導入**

**1. TortoiseGitをインストール**
* https://tortoisegit.org/download/ 
* [*Download TortoiseGit 2.7.0*] (32bitか64bitを選ぶ)をクリックし、ダウンロードできたら実行してください。
* 設定がたくさん出てきますが全部OKで構いません。

**2. Windows版Gitをインストール**
* https://git-scm.com/download/win 
* 32bitか64bitを選んでください。
* こっちも全部OKでいいです。

**3. レポジトリ作成**
* 素材を置きたいフォルダで右クリック、[Git Clone]をクリック
* なんかウィンドウが出てくるので[URL]に　**git@gitlab.com:Aineias/pri-chan-bb.git**　をコピペ
* OKを押すと「pri-chan-bb」というフォルダが作成されます。



## **更新　(Fetch)**

「pri-chan-bb」を右クリック、[TortoiseGit]　>　[Fetch]をクリックし、OKを押すと最新版に更新されます <br/>
（素材パックを更新する時はTwitterでお知らせします）

## **素材の追加(Merge Request)**

やり方を知ってる人はご自由にどうぞ。

* GitLabのページ(**https://gitlab.com/Aineias/pri-chan-bb**) に行きForkを作成
* Branchをcheckoutし、変更を行う。
* 変更をCommitし、Pushする。
* GitLabでBranchの画面に行ってMerge Requestを出す。

難しそうでしたら私のツイッターに直接送り付けてもいいです（本末転倒）

